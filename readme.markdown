AndroidPMDFiles
==============

AndroidPMDFiles, pmd files for android java application development.

NOTE
=====

Main pmd file has rules from the alpha/beta PMD back ported to the stable
pmd release.

Main pmd file included but if targeting Amazon KindleFire you may want to copy
the main file and modify it to check for stuff that is disallowed by amazon for the 
KindleFile device.

Project License
============

Apache License 2.0

Project Lead
==========

Fred Grott, his blogs:

[Fred Grott-posterous](http://fredgrott.posterous.com)

[Shareme-githubpages](http://shareme.github.com)


